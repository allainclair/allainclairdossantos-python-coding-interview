import json

from marshmallow import Schema, fields

DATA_PATH = 'data/users.json'


class User:
    def __int__(self):
        pass

    @classmethod
    def get_all(cls):
        user_schema = UserSchema(many=True)
        with open(DATA_PATH) as f:
            user_objects = json.load(f)
            user_objects = filter_users(user_objects)
            return user_schema.dump(user_objects)


class UserSchema(Schema):
    id = fields.Int()
    first_name = fields.Str()
    last_name = fields.Str()
    email = fields.Str()
    gender = fields.Str()
    ip_address = fields.Str()
    birth_date = fields.Str()


def filter_users(user_objects):
    return [
        user
        for user in user_objects
        if user['email'] and user['birth_date'] and user['gender']
    ]
